public class FindMin {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);
        int result;
        if (a < b && a < c) {
            result = a;

        } else if (b < a && b < c) {
            result = b;

        } else {
            result = c;

        }
        System.out.println("MIN:" + result);

    }
}