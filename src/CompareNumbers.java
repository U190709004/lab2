public class CompareNumbers {

    public static void main(String[] args){
        System.out.println();
        int value1 = 2;
        int value2 = 2;

        if (value1 < value2) {
            System.out.println(value1 + " < " + value2);
        }else if (value1 == value2) {
            System.out.println(value1 + " = " + value2);
        }else {
            System.out.println(value1 + " > " + value2);
        }

    }
}
